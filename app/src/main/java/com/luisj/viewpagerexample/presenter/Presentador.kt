package com.luisj.viewpagerexample.presenter

import android.content.Context
import android.util.Log
import com.luisj.viewpagerexample.database.DataBases
import com.luisj.viewpagerexample.interfaces.PresenterCookie
import com.luisj.viewpagerexample.interfaces.PresenterRecycler

class Presentador(private val context: Context, private val presenterRecycler: PresenterRecycler

) : PresenterCookie {
    fun init() {
        getCookie()
    }


    override fun getCookie() {
        val db = DataBases(context)
        val cart = db.getCart()
        if(cart.size>0){
            viewCookie(cart)
            Log.d("tag","size cero")
        }else{
            Log.d("tag","size cero")

        }
    }

    override fun viewCookie(cart:ArrayList<Pair<String,String>>) {
        presenterRecycler.initAdapter(presenterRecycler.createAdapter(cart))
        presenterRecycler.generateLinearLayout()
    }
}