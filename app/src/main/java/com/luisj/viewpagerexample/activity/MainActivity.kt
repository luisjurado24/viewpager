package com.luisj.viewpagerexample.activity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.luisj.viewpagerexample.R
import com.luisj.viewpagerexample.database.DataBases
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var broadcastReceiver:BroadcastReceiver?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        installListener()
        val intentWeb = Intent(this, WebView::class.java)
        btn_one.setOnClickListener {
            val url = "https://www.whatsok.net/preciosg/index.php"
            intentWeb.putExtra(getString(R.string.btn_text_one), url)
            startActivity(intentWeb)
        }

    }

    private fun createAlertDialog(b: Boolean) {
        btn_one.isEnabled= b
    }

    private fun toastMake(msg:String,enable:Boolean){
        btn_one.isEnabled= enable
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }

    private fun installListener(){
        val broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val extras = intent.extras
                val info = extras!!.getParcelable<Parcelable>("networkInfo") as NetworkInfo?
                val state: NetworkInfo.State = info!!.state
                if(state== NetworkInfo.State.DISCONNECTED){
                    createAlertDialog(false)
                }else{
                    toastMake("Conectado",true)
                }
                Log.d("InternalBadcastReceiver", info.toString() + " " + state.toString())
            }
        }
        val intentFilter = IntentFilter()
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        registerReceiver(broadcastReceiver, intentFilter)
    }

    override fun onDestroy() {
        unregisterReceiver(broadcastReceiver)
        super.onDestroy()
    }

}