package com.luisj.viewpagerexample.data

import org.json.JSONObject

data class Cookie(val host:String, val jsonCookie:JSONObject)