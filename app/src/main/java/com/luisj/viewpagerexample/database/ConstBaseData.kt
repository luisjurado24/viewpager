package com.luisj.viewpagerexample.database

class ConstBaseData {


    companion object {
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "cookies"

        const val TABLE_CART = "Cookies"
        const val TABLE_CART_HOST = "columnhost"
        const val TABLE_CART_ID = "id"
        const val TABLE_CART_COOKIE = "cookie"

    }
}

