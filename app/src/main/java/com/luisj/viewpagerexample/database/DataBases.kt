package com.luisj.viewpagerexample.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.webkit.WebViewClient

class DataBases(private val context: Context): SQLiteOpenHelper(context,ConstBaseData.DATABASE_NAME,null,ConstBaseData.DATABASE_VERSION) {
    override fun onCreate(p0: SQLiteDatabase?) {
        val query = "CREATE TABLE ${ConstBaseData.TABLE_CART} ( ${ConstBaseData.TABLE_CART_ID} TEXT," +
                "${ConstBaseData.TABLE_CART_COOKIE} TEXT, ${ConstBaseData.TABLE_CART_HOST} text)"
        p0!!.execSQL(query)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        val query = "DROP TABLE IF EXISTS ${ConstBaseData.TABLE_CART} "
        p0!!.execSQL(query)
        onCreate(p0)
    }
    public fun getCart(): ArrayList<Pair<String,String>> {
        val query = "SELECT * FROM ${ConstBaseData.TABLE_CART}"
        val cookiesArray: ArrayList<Pair<String,String>> = ArrayList()
        val db = this.writableDatabase
        val registros = db.rawQuery(query, null)
        while (registros.moveToNext()) {
            val pairString = Pair(registros.getString(2),registros.getString(1))
            cookiesArray.add(pairString)
        }

        return cookiesArray
    }
    fun getLastId(): Int{
        val query = "SELECT ${ConstBaseData.TABLE_CART_ID} FROM ${ConstBaseData.TABLE_CART}"
        val cookiesArray: ArrayList<Int> = ArrayList()
        val db = this.writableDatabase
        val registros = db.rawQuery(query, null)
        while (registros.moveToNext()) {
            cookiesArray.add(registros.getInt(0))
        }
        db.close()
        var lastIndex = 0
        lastIndex = try{
            cookiesArray.last()+1
        }catch (e:Exception){
            0
        }
        return lastIndex
    }
    public fun deleteAll(){
        val db = this.writableDatabase
        db.delete(ConstBaseData.TABLE_CART,"",null)
        db.close()
    }
    public fun deleteJson(id: String):Int {
        val db = writableDatabase
        val result= db.delete(ConstBaseData.TABLE_CART,"id=\"$id\"",null)
        db.close()
        return result
    }

    public fun setJson(contentValues: ContentValues) {
        val db = writableDatabase
        db.insert(ConstBaseData.TABLE_CART, null, contentValues)
        db.close()
    }


}