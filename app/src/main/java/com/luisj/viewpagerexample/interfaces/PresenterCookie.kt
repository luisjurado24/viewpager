package com.luisj.viewpagerexample.interfaces

interface PresenterCookie {
    fun getCookie()
    fun viewCookie(cart:ArrayList<Pair<String,String>>)

}