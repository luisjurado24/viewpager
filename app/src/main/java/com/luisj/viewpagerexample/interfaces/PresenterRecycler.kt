package com.luisj.viewpagerexample.interfaces

import com.luisj.viewpagerexample.adapter.AdapterCookie
import org.json.JSONObject

interface PresenterRecycler {
    fun generateLinearLayout()
    fun createAdapter(record: ArrayList<Pair<String,String>>): AdapterCookie
    fun initAdapter(adapter: AdapterCookie)
}