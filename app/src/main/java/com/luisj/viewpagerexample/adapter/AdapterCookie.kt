package com.luisj.viewpagerexample.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.luisj.viewpagerexample.R
import com.luisj.viewpagerexample.data.Cookie
import org.json.JSONObject

class AdapterCookie(private val context: Context, private val records: ArrayList<Cookie>) :
    RecyclerView.Adapter<AdapterCookie.CookieViewHolder>() {
    inner class CookieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bin(cookie: Cookie){
                val id = itemView.findViewById<TextView>(R.id.id_cookie)
                val json = itemView.findViewById<TextView>(R.id.cookie_json)
                id.text = cookie.host
                json.text = cookie.jsonCookie.toString()
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CookieViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.card_view_cookie, parent, false)
        return CookieViewHolder(view)
    }

    override fun onBindViewHolder(holder: CookieViewHolder, position: Int) {
        val record = records[position]
        CookieViewHolder(holder.itemView).bin(record)
    }

    override fun getItemCount(): Int {
        return records.size
    }
}